source 'https://rubygems.org'
ruby '2.3.0'

gem 'autoprefixer-rails'                                      # -> Autoprefixer parses CSS and adds vendor prefixes based on "Can I Use"
gem 'bourbon'                                                 # -> A simple and lightweight mixin library for Sass
gem 'devise'                                                  # -> Flexible Authentication for Rails
gem 'jquery-rails'                                            # -> Use jquery as the JavaScript library
gem 'neat'                                                    # -> A lightweight, semantic grid framework built with Bourbon
gem 'normalize-rails'                                         # -> Integrates normalize.css with the rails asset pipeline
gem 'puma'                                                    # -> Web Server built for concurrency
gem 'rails', '~> 4.2.5.0'                                     # -> Specify Rails Version
gem 'sass-rails', '~> 5.0'                                    # -> Use SCSS for stylesheets
gem 'slim-rails'                                              # -> View replacement for ERB templates
gem 'sqlite3'                                                 # -> Use sqlite3 as the database for Active Record
gem 'turbolinks'                                              # -> Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'uglifier', '>= 1.3.0'                                    # -> Use Uglifier as compressor for JavaScript assets

# gem 'activeadmin', github: 'activeadmin'                      # -> Framework for creating administrative backends in rails
# gem 'active_model_serializers'                                # -> ActiveModel::Serializer brings convention over configuration to your JSON generation
# gem 'administrate'                                            # -> Admin Framework for rails, aims to provide a better user experience for site admins and developers.
# gem 'airbrake'                                                # -> Ruby gem for reporting exception errors to Airbrake
# gem 'bcrypt', '~> 3.1.7'                                      # -> Use ActiveModel has_secure_password
# gem 'bitters'                                                 # -> Add pre-defined styles to bourbon
# gem 'coffee-rails', '~> 4.1.0'                                # -> Use CoffeeScript for .coffee assets and views
# gem 'font-awesome-rails'                                      # -> Font-Awesome font bundled as an asset for the rails pipeline
# gem 'health_check'                                            # -> Simple health check of Rails apps for use with services like Pingdom
# gem 'honeybadger'                                             # -> Ruby gem for reporting errors to honeybadger.io
# gem 'httparty'                                                # -> Easily build classes that can use Web-based APIs and related services
# gem 'interactor-rails'                                        # -> Provides a common interface for performing complex user interactions
# gem 'newrelic_rpm'                                            # -> New Relic RPM Ruby Agent http://www.newrelic.com
# gem 'pg'                                                      # -> Use postgresql as the database for Active Record
# gem 'raygun4ruby'                                             # -> Ruby adapter for the Raygun error reporter
# gem 'resque'                                                  # -> A Redis-backed Ruby library for creating background jobs, placing them on multiple queues, and processing them later
# gem 'recipient_interceptor'                                   # -> Intercept recipients when delivering email through rails
# gem 'scout_apm'                                               # -> Ruby monitor for reporting performance metrics to Scout
# gem 'sidekiq'                                                 # -> Simple, efficient background processing for Ruby http://sidekiq.org
# gem 'simple_form'                                             # -> Forms made easy for Rails! It's tied to a simple DSL, with no opinion on markup
# gem 'sdoc', '~> 0.4.0', group: :doc                           # -> bundle exec rake doc:rails generates the API under doc/api
# gem 'therubyracer', platforms: :ruby                          # -> See https://github.com/rails/execjs#readme for more supported runtimes

group :development, :test do
  gem 'byebug'                                                # -> Call 'byebug' anywhere in the code to stop execution and get a debugger console
end

group :development do
  gem 'annotate'                                              # -> Annotate Models.  run with `annotate` || `annotate --routes`
  gem 'awesome_print', require: 'ap'                          # -> Pretty print your Ruby objects with style
  gem 'better_errors'                                         # -> A better error page for Rails
  gem 'binding_of_caller'                                     # -> Advanced features for better_errors
  gem 'brakeman', require: false                              # -> Checks application for security vulnerabilities, run with `brakeman`
  gem 'bullet'                                                # -> Bullet gem is designed to help you increase your application's performance by reducing the number of queries it makes.
  gem 'guard'                                                 # -> Command line tool to easily handle events on file system modifications.
  gem 'guard-brakeman', require: false                        # -> Guard plugin for running brakeman scans
  gem 'guard-livereload', require: false                      # -> Guard Plugin for livereloading the browser, needs rack-livereload
  gem 'guard-minitest', require: false                        # -> Guard Plugin for running Minitest
  gem 'guard-rubocop', require: false                         # -> Guard Plugin for running rubocop scans
  gem 'meta_request'                                          # -> Chrome extension for Rails development using RailsPanel
  gem 'pry'                                                   # -> An IRB alternative and runtime developer console
  gem 'pry-rails'                                             # -> Defaults Rails Console to Pry instead of IRB
  gem 'pry-byebug'                                            # -> Pry navigation commands via byebug
  gem 'quiet_assets'                                          # -> Mutes assets pipeline log messages
  gem 'rack-livereload'                                       # -> Bring in livereload.js into Rack middleware
  gem 'rails_best_practices', require: false                  # -> A Rails code analyzer for quality, run with `rails_best_practices`
  gem 'rubocop', require: false                               # -> A Ruby static code analyzer, run with `rubocop`
  gem 'rubycritic', require: false                            # -> Ruby code quality report, run with `rubycritic`
  gem 'terminal-notifier', :require => false                  # -> Send User Notifications on Mac OS X from the command-line
  gem 'terminal-notifier-guard', :require => false            # -> Mac OS X User Notifications for Guard
  gem 'traceroute'                                            # -> A Rake task that helps you find dead routes and unused actions in your Rails app. `rake traceroute`
  gem 'web-console', '~> 3.0'                                 # -> Access an IRB console on exception pages or by using the `console` tag in views
  gem 'zeus', require: false                                  # -> Boot any rails app in under a second, guard errors without this in the gemfile...

  # gem 'bundler-audit', require: false                         # -> Patch-level verification for Bundler
  # gem 'guard-pow', require: false                             # -> Guard Plugin for running POW server
  # gem 'letter_opener'                                         # -> Preview mail in the browser instead of sending.
  # gem 'mailcatcher'                                           # -> MailCatcher runs a super simple SMTP server which catches any message sent to it to display in a web interface.
  # gem 'pry-git'                                               # -> A Ruby Aware GIT Interface
  # gem 'pry-rescue'                                            # -> Start a pry session whenever something goes wrong
  # gem 'rack-mini-profiler'                                    # -> Profiler for your development and production Ruby rack apps.
  # gem 'rainbow'                                               # -> Color printed text in ANSI terminals
  # gem 'spring'                                                # -> Spring speeds up development by keeping your application running in the background.  Read more: https://github.com/rails/spring
end

group :test do
  gem 'shoulda-matchers'                                    # -> Provides RSpec- and Minitest- compatible one-liners that test common Rails functionality
  gem 'simplecov', require: false                             # -> SimpleCov is a code coverage analysis tool for Ruby.

  # gem 'capybara-webkit'                                     # -> A Capybara driver for headless WebKit to test JavaScript web apps
  # gem 'database_cleaner'                                    # -> Strategies for cleaning databases in Ruby. Can be used to ensure a clean state for testing
  # gem 'factory_girl_rails'                                  # -> Fixtures replacement with a straightforward definition syntax, support for multiple build strategies
  # gem 'launchy'                                             # -> Helper for launching cross-platform applications in a fire and forget manner
  # gem 'timecop'                                             # -> A unified method to mock Time.now, Date.today, and DateTime.now in a single call
end

group :staging, :production do
  gem 'lograge'                                               # -> An attempt to tame Rails' default policy to log everything
  gem 'rails_12factor'                                        # -> 12Factor gem recommended for Heroku
  gem "rack-timeout"                                          # -> Abort requests that are taking too long

  # gem 'heroku-deflater'                                       #-> Enable gzip compression on heroku, but don't compress images
  # gem "rails_stdout_logging"                                  # -> Heroku Rails gem to configure your app to log to standard out
  # gem 'skylight'                                              # -> Skylight Ruby Agent to monitor application
end
