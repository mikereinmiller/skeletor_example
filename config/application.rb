require File.expand_path('../boot', __FILE__)

require 'rails/all'
Bundler.require(*Rails.groups)

module SkeletorExample
  class Application < Rails::Application
    # Enable/Disable QuietAssets gem
    config.quiet_assets = true

    # Disable generation of helpers, javascripts, css, and view specs
    config.generators do |generate|
      generate.helper false
      generate.assets false
      generate.view_specs false
    end

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    # Load ENV Variables if File Exist
    config.before_configuration do
      env_file = File.join(Rails.root, 'config', 'env.yml')

      if File.exists?(env_file)
        YAML.load(File.open(env_file))[Rails.env].each do |key, value|
          ENV[key.to_s] = value
        end
      end
    end

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    
  end
end
