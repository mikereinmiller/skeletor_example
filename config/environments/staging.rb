# Staging configuration is identical to production, with some overrides

require_relative './production'

# Intercept recipients when delivering email with the Mail gem.
# Mail.register_interceptor(
#   RecipientInterceptor.new(ENV.fetch('EMAIL_RECIPIENTS'))
# )

Rails.application.configure do
  config.action_mailer.default_url_options = { host: 'staging.skeletor_example.com' }

  config.action_mailer.smtp_settings = {
    # Add Staging Mailer details if different?
  }
end
