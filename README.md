# SkeletorExample

TODO: Update this summary for details about the project.


# Getting Started
This project uses the latest version of Ruby, Rails and Postgresql.  If you are
developing and prefer not to deal with installing all the dependencies, then you
should looking into https://www.nitrous.io/

1. The environment variables for the project are setup in a env.yml file.  It
  is recommended to copy and rename the env.example file to env.yml.
  The YAML file should *not* be commited to git.
2. Update the appropriate keys in the env.yml file (like db settings and api keys).
3. Run `bundle install` to install all the project dependencies.
4. Run `rake db:create` to create your database.
5. Run `rake db:migrate` to setup the correct database structure
6. Run `rake db:seed` to seed the database for local development.
7. Run `rails s` to start your rails server and view the project by going to
  localhost:3000.  If you are using nitrous you will need to bind to a specific
  ip `rails s -b 0.0.0.0`.


# Development
We want to keep are apps clean and easily maintainable, so besides from abiding
by the style guides below.  We want to run a couple reports to help us code
better and catch any security flaws we might have created.

After you have finished coding and are ready to commit, remember to run the
following:

  * Run any test created for the project `rake test`.
  * Check the simplecov report at `coverage/index.html` to see what you might
  have missed.
  * Run `rubocop` to analyze your code.
  * Run `rails_best_practices` to check quality of rails code.
  * Run `brakeman` for security vulnerabilities.
  * Run `rubycritic` to generate a quality report to view.
  * Run `rake traceroute` to view used/unused routes.

Hopefully the above all looks great and passed, we will use these to help with
our code review process.


# Style Guides
* http://cssguidelin.es/
* https://github.com/bbatsov/ruby-style-guide
* https://github.com/bbatsov/rails-style-guide
* TODO:  Find JS guidelines
