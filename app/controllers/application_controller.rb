class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  helper_method :present
  def present(object, klass = nil)
    klass ||= object.respond_to?(:size) ? object.first.class : object.class
    presenter = "#{klass}Presenter".constantize.present(object, view_context)
    yield presenter if block_given?
    presenter
  end
end
