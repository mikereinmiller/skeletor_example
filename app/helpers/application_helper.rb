module ApplicationHelper
  def display_meta_tag(tag, default)
    if tag == :title
      content_tag(tag, meta_tag_content(tag, default))
    else
      tag(:meta, name: tag, content: meta_tag_content(tag, default))
    end
  end

  def meta_tag(tag, text)
    provide(tag, text)
  end

  def meta_tags(tags)
    tags.each do |tag, text|
      provide(tag, text)
    end
  end

  def title(text)
    meta_tag(:title, text)
  end

  def description(text)
    meta_tag(:description, text)
  end

  def keywords(text)
    meta_tag(:keywords, text)
  end

  private

  def meta_tag_content(tag, default)
    content_for?(tag) ? content_for(tag) : default
  end
end
