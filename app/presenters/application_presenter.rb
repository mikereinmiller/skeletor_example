class ApplicationPresenter < SimpleDelegator
  def initialize(object, view_context)
    super(object)
    @view_context = view_context
  end

  def self.wrap(collection, view_context)
    collection.map do |obj|
      new obj, view_context
    end
  end

  def self.present(collection, view_context)
    if collection.respond_to?(:size)
      wrap collection, view_context
    else
      new collection, view_context
    end
  end

  private

  # "Present" Objects by class name variable"
  # i.e. `presents :user` on UserPresenter
  def presents(name)
    define_method(name) do
      object
    end
  end

  def h
    @view_context
  end

  def object
    __getobj__
  end
end
