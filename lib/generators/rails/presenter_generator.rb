module Rails
  module Generators
    class PresenterGenerator < NamedBase
      source_root File.expand_path('../templates', __FILE__)
      check_class_collision suffix: 'Presenter'

      def create_presenter
        template 'presenter.rb', File.join(
          'app/presenters',
          class_path,
          "#{file_name}_presenter.rb"
        )
      end

      hook_for :test_framework
    end
  end
end
